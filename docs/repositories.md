Packages provided by the kmods SIG are seperated into different repositories.
Install `centos-release-kmods` as described in [Quickstart](install.md) to gain access to these repositories.

## Main
Packages released in `main` are designed to be extensions to the distribution.
This repository is enabled by default.

## Rebuild
Packages released in `rebuild` modify content provided by the distribution.
In particular the source code for these kernel modules has been modified to restore support for devices that have been explicitly removed.
This repository can be enabled by:

```shell
$ sudo dnf config-manger --set-enabled centos-kmods-rebuild
```
