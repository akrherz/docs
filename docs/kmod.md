RPM packages list provided kernel modules as `Provides`.
I.e., one can use `dnf` to search for packages in enabled repositories which provide a certain kernel module named `name.ko`:

```
sudo dnf whatprovides "kmod(name.ko)"
```
