The SIG publishes quarterly reports on the [CentOS Blog](https://blog.centos.org).

# 2022
* [2022Q2](https://blog.centos.org/2022/07/centos-community-newsletter-july-2022/)
* [2022Q1](https://blog.centos.org/2022/04/centos-community-newsletter-april-2022/)

# 2021
* [2021Q4](https://blog.centos.org/2022/01/centos-community-newsletter-january-2022/)
* [2021Q3](https://blog.centos.org/2021/10/kmods-sig-quarterly-report-2/)
* [2021Q2](https://blog.centos.org/2021/07/kmods-sig-quarterly-report/)
