# Prebuilt Driver Discs
The Kmods SIG is currently looking into building Driver Disc in CBS.
This requires collaboration and further discussion with Infra SIG.
The current state can be tracked [here](https://pagure.io/centos-infra/issue/418).

# Driver Disc Creator
The Kmods SIG provides a tool called [Driver Disc Creator](https://gitlab.com/CentOS/kmods/ddc) to create driver discs including kernel module packages specified by the user.
It is currently limited to kernel module packages provided by the Kmods SIG.
Run `ddc.sh --help` for information on usage.
