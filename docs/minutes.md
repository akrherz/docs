This page collects meeting minutes for the Kmods SIG.
See [Regular Meetings](communication.md#regular-meetings) for schedule of regular meetings.

# 2022
* [2022-09-05](https://www.centos.org/minutes/2022/September/centos-meeting.2022-09-05-16.00.html)
* [2022-08-01](https://www.centos.org/minutes/2022/August/centos-meeting.2022-08-01-15.00.html)
* [2022-07-04](https://centbot.centos.org/minutes/2022/July/centos-meeting.2022-07-04-15.00.html)
* [2022-06-06](https://centbot.centos.org/minutes/2022/June/centos-meeting.2022-06-06-15.00.html)
* [2022-05-02](https://www.centos.org/minutes/2022/May/centos-meeting.2022-05-02-15.00.html)
* [2022-03-07](https://www.centos.org/minutes/2022/March/centos-meeting.2022-03-07-16.00.html)
* [2022-02-21](https://www.centos.org/minutes/2022/February/centos-meeting.2022-02-21-16.00.html)
* [2022-02-07](https://www.centos.org/minutes/2022/February/centos-meeting.2022-02-07-16.00.html)
* [2022-01-24](https://www.centos.org/minutes/2022/January/centos-meeting.2022-01-24-16.00.html)
* [2022-01-10](https://www.centos.org/minutes/2022/January/centos-meeting.2022-01-10-16.00.html)

# 2021
* [2021-12-13](https://www.centos.org/minutes/2021/December/centos-meeting.2021-12-13-16.00.html)
* [2021-11-29](https://www.centos.org/minutes/2021/November/centos-meeting.2021-11-29-15.02.html)
* [2021-11-15](https://www.centos.org/minutes/2021/November/centos-meeting.2021-11-15-15.00.html)
* [2021-11-01](https://www.centos.org/minutes/2021/November/centos-meeting.2021-11-01-15.00.html)
* [2021-10-18](https://www.centos.org/minutes/2021/October/centos-meeting.2021-10-18-15.00.html)
* [2021-10-04](https://www.centos.org/minutes/2021/October/centos-meeting.2021-10-04-15.00.html)
* [2021-09-20](https://www.centos.org/minutes/2021/September/centos-meeting.2021-09-20-15.00.html)
* [2021-09-06](https://www.centos.org/minutes/2021/September/centos-meeting.2021-09-06-15.04.html)
* [2021-08-23](https://www.centos.org/minutes/2021/August/centos-meeting.2021-08-23-15.00.html)
* [2021-08-09](https://www.centos.org/minutes/2021/August/centos-meeting.2021-08-09-15.00.html)
* [2021-07-26](https://www.centos.org/minutes/2021/July/centos-meeting.2021-07-26-15.04.html)
* [2021-07-12](https://www.centos.org/minutes/2021/July/centos-meeting.2021-07-12-15.00.html)
* [2021-06-28](https://www.centos.org/minutes/2021/June/centos-meeting.2021-06-28-15.11.html)
