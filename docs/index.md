The [Kmods SIG](https://wiki.centos.org/SpecialInterestGroup/Kmods) focuses on packaging and maintaining kernel modules for CentOS Stream and Enterprise Linux.

We welcome anybody that's interested and willing to do work within the scope of the Kmods SIG to join and contribute.
See [membership](membership.md) for how to join.
