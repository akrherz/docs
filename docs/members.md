# Current members

* [Peter Georg](https://wiki.centos.org/PeterGeorg)
* [Patrick Riehecky](https://wiki.centos.org/PatrickRiehecky)

# Chairs
The SIG is co-chaired by two equal chairpersons elected by SIG members for one year.
Each chairperson is elected individually using a plurality vote.

The SIG is currently co-chaired by Peter Georg and <to be elected\>.

The next SIG chair election is scheduled to be held in June 2022.

# Former members
* Jonathan Billings
