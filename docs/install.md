We offer `centos-release-kmods` packages containing our repository configuration files and [public package signing key](https://www.centos.org/keys/RPM-GPG-KEY-CentOS-SIG-Kmods) for different Enterprise Linux major versions.

For convenience most Enterprise Linux based distributions include a `centos-release-kmods` package in their default repositories ready to be installed:

```shell
$ sudo dnf install centos-release-kmods
```

Once `centos-release-kmods` is installed individual repositories can be enabled as desired.
See [Repositories](repositories.md) for a list of available repositories and futher information.
