Everybody is welcome to join and contribute to the Kmods SIG.
Membership can be requested by asking on the [mailing list](communication.md#mailing-list), the [Kmods SIG Issue Tracker](https://gitlab.com/CentOS/kmods/sig/-/issues), or by asking during one of the [regular meetings](communication.md#regular-meetings).
Any current member can raise objections and request a simple majority vote on membership applications.
SIG members are expected to actively contribute or otherwise remain engaged with the project.
Stale members may be removed by a simple majority vote after six months of inactivity.
