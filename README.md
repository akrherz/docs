# Kmods SIG documentation

This git repository contains the source files required to generate the Kmods SIG documentation hosted at: https://sigs.centos.org/kmods/

It uses [mkdocs](https://mkdocs.org) but with a specific [material](https://squidfunk.github.io/mkdocs-material) theme.

## How to test locally
1. Install requirements via pip using the following command:
```
pip install -r requirements.txt
```
Depending on your environment, the `pip` command may be called `pip3`.

2. Run MkDocs' builtin development web server:
```
mkdocs serve
```

3. Open a browser and navigate to http://localhost:8000 to see your updates.
